import React, { Component} from "react";
import {hot} from "react-hot-loader";
import "./App.css";
// import lifecycle from './images/lifecycle.png';
// import Header from './../dist/Header.umd.js';
import HeaderComponent from 'mmt-header';

class App extends Component{
  render(){
    return(
      <div>
      <HeaderComponent />
      {/* <div className="App">
        <h1> Hello, Sourav ! </h1>
        <div> <img src={lifecycle} /></div>
      </div> */}
      </div>
    );
  }
}

export default hot(module)(App);